<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package uau
 */

?>


<article class="search row" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="col-5 col-md-4 col-lg-3">
			<?php uau_post_thumbnail(); ?>
		</div>
		<div class="searchRightArea col-7 col-md-6 col-lg-7">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			<?php the_excerpt(); ?>
		</div>
</article><!-- #post-<?php the_ID(); ?> -->


