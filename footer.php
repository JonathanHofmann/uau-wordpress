<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package uau
 */

?>
	<footer id="colophon" class="site-footer">
			<div class="horizontalBorder col-md-10"></div>
			<div></div>
			<div class="main-navigation">
				<ul>
					<li>
						<a href="https://jonathan.undefiniert.ch/kontakt">Kontakt</a>
					</li>
					<li>
						<a href="https://jonathan.undefiniert.ch/impressum">Impressum</a>
					</li>
				</ul>	
			</div>
			<a class="SocialMediaIcon" href="https://www.instagram.com" target="blank"><img src="/wp-content/uploads/2020/09/Instagram.svg" alt=""></a>
			<a class="SocialMediaIcon" href="https://www.facebook.com" target="blank"><img src="/wp-content/uploads/2020/09/FaceBook.svg" alt=""></a>
			<a class="SocialMediaIcon" href="https://www.youtube.com" target="blank"><img src="/wp-content/uploads/2020/09/YouTube.svg" alt=""></a>
		</footer><!-- #colophon -->
	</div><!-- #page -->
<?php wp_footer(); ?>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="js/bootstrap_classes.js"></script>
</body>
</html>
